package pl.sda.projektkoncowy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RentalDto {

    private UUID idRental;
    private LocalDateTime rentalDate;
    private UUID reservationID;
    private String comments;

    //Dodanie konstrukora z ID w celu stworzenia CarRental z parametrem "(UUID id)" w CarRentalService
    public RentalDto(UUID idRental) {
        this.idRental = idRental;
    }

    public RentalDto(LocalDateTime rentalDate, UUID reservationID, String comments) {
        this.rentalDate = rentalDate;
        this.reservationID = reservationID;
        this.comments = comments;
    }
}
