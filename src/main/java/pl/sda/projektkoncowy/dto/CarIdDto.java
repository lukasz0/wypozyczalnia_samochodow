package pl.sda.projektkoncowy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class CarIdDto {
    private UUID id;
}
