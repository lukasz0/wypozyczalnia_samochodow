package pl.sda.projektkoncowy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.sda.projektkoncowy.entity.Reservation;

import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReturnDto {

    private UUID returnId;
    private LocalDateTime returnDate;
    private UUID reservationID;
    private BigDecimal surcharge;

    public ReturnDto(LocalDateTime returnDate, UUID reservationID, BigDecimal surcharge) {
        this.returnDate = returnDate;
        this.reservationID = reservationID;
        this.surcharge = surcharge;
    }
}
