package pl.sda.projektkoncowy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.sda.projektkoncowy.entity.Branch;
import pl.sda.projektkoncowy.entity.Car;
import pl.sda.projektkoncowy.entity.Employee;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarRentalDto {
    private String name;
    private String website;
    private String phoneNumber;
    private String owner;
    private String logoType;

    private List<RegisterBranchForm> branchList;
    private List<CarDto> carList;
    private List<EmployeeDto> employeeList;


}
