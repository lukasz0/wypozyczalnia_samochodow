package pl.sda.projektkoncowy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
public class RegisterCompanyForm {

    private final String email;
    private final String vat;
    private final String name;
    private final String password;
    private final AddressDto addressDto;

    public RegisterCompanyForm(String email, String vat, String name,String password,AddressDto addressDto) {
        this.email = Objects.requireNonNull(email);
        this.vat = Objects.requireNonNull(vat);
        this.name = Objects.requireNonNull(name);
        this.password = Objects.requireNonNull(password);
        this.addressDto=Objects.requireNonNull(addressDto);
    }
}
