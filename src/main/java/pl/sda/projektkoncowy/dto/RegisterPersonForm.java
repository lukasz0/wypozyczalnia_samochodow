package pl.sda.projektkoncowy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RegisterPersonForm {
    private String firstName;
    private String lastName;
    private String email;
    private String pesel;
    private String password;
    private final AddressDto addressDto;
}
