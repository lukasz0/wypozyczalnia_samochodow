package pl.sda.projektkoncowy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.sda.projektkoncowy.enumarated.JobTitle;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@NoArgsConstructor
public class EmployeeDto {
    private String firstName;
    private String lastName;
    @Enumerated(EnumType.STRING)
    private JobTitle jobTitle;
    private String password;
    private RegisterBranchForm registerBranchForm;

    public EmployeeDto(String firstName, String lastName, JobTitle jobTitle, RegisterBranchForm registerBranchForm) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.jobTitle = jobTitle;
        this.registerBranchForm = registerBranchForm;
    }

    public EmployeeDto(String firstName, String lastName, JobTitle jobTitle, String password, RegisterBranchForm registerBranchForm) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.jobTitle = jobTitle;
        this.password = password;
        this.registerBranchForm = registerBranchForm;
    }
}
