package pl.sda.projektkoncowy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.sda.projektkoncowy.entity.Address;
import pl.sda.projektkoncowy.entity.Car;
import pl.sda.projektkoncowy.entity.Employee;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterBranchForm {

    private AddressDto addressDto;
    private List<EmployeeDto> employeeListDto;
    private List<CarDto> carListDto;


    public RegisterBranchForm(AddressDto addressDto, List<CarDto> carListDto) {
        this.addressDto = addressDto;
        this.carListDto = carListDto;
    }
}
