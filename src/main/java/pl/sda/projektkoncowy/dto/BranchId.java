package pl.sda.projektkoncowy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;
import java.util.UUID;

@Data
@NoArgsConstructor
public class BranchId {
    private UUID id;
    public BranchId(UUID id){
        this.id= Objects.requireNonNull(id);
    }
}
