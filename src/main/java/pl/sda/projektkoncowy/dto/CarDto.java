package pl.sda.projektkoncowy.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.sda.projektkoncowy.enumarated.Category;
import pl.sda.projektkoncowy.enumarated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarDto {

    private UUID id;
    private String carBrand;
    private String model;
    private String bodyType;
    private String year;
    private String color;
    private String mileage;
    private Long price;

    private List<ReservationDto> reservationListDto = new ArrayList<>();

    private RegisterBranchForm registerBranchForm;

    private UUID idBranch;

    @Enumerated(EnumType.STRING)
    private Status status;
    @Enumerated(EnumType.STRING)
    private Category category;


    public CarDto(UUID id, String carBrand, String model, String bodyType, String year, String color, String mileage,
                  Long price, Status status, Category category) {
        this.id = id;
        this.carBrand = carBrand;
        this.model = model;
        this.bodyType = bodyType;
        this.year = year;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
        this.status = status;
        this.category = category;
    }
    public CarDto(UUID id, String carBrand, String model, String bodyType, String year, String color, String mileage, Long price, List<ReservationDto> reservationListDto, UUID idBranch, Status status, Category category) {
        this.id = id;
        this.carBrand = carBrand;
        this.model = model;
        this.bodyType = bodyType;
        this.year = year;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
        this.reservationListDto = reservationListDto;
        this.idBranch = idBranch;
        this.status = status;
        this.category = category;
    }


    public CarDto(String carBrand, String model, String bodyType, String year, String color, String mileage, Long price, List<ReservationDto> reservationListDto, UUID idBranch, Status status, Category category) {

        this.carBrand = carBrand;
        this.model = model;
        this.bodyType = bodyType;
        this.year = year;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
        this.reservationListDto = reservationListDto;
        this.idBranch = idBranch;
        this.status = status;
        this.category = category;
    }

    public CarDto(UUID id, String carBrand, String model, String bodyType, String year, String color, String mileage, Long price, List<ReservationDto> reservationListDto, RegisterBranchForm registerBranchForm, Status status, Category category) {
        this.id = id;
        this.carBrand = carBrand;
        this.model = model;
        this.bodyType = bodyType;
        this.year = year;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
        this.reservationListDto = reservationListDto;
        this.registerBranchForm = registerBranchForm;
        this.status = status;
        this.category = category;
    }

    public CarDto( String carBrand, String model, String bodyType, String year, String color, String mileage, Long price, List<ReservationDto> reservationListDto, RegisterBranchForm registerBranchForm, Status status, Category category) {

        this.carBrand = carBrand;
        this.model = model;
        this.bodyType = bodyType;
        this.year = year;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
        this.reservationListDto = reservationListDto;
        this.registerBranchForm = registerBranchForm;
        this.status = status;
        this.category = category;
    }
}
