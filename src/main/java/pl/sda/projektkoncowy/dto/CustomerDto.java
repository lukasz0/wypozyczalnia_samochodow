package pl.sda.projektkoncowy.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class CustomerDto {

    private UUID id;
    private final String email;
    private String name;
    private String firstName;
    private String LastName;
    private String vat;
    private String pesel;
    private AddressDto addresses;
    private List<ReservationDto> reservationDtos;

    public CustomerDto(UUID id, String email, String name) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.reservationDtos=new ArrayList<>();
    }

    public CustomerDto(UUID id, String email,AddressDto addresses) {
        this.id = id;
        this.email = email;
        this.reservationDtos=new ArrayList<>();
    }

    public CustomerDto(UUID id, String email, String name, String vat, AddressDto addresses) {
        this(id,email,name);
        this.vat = vat;
        this.addresses = addresses;
        this.reservationDtos=new ArrayList<>();
    }

    public CustomerDto(UUID id, String email, String firstName, String lastName, String pesel, AddressDto addresses) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        LastName = lastName;
        this.pesel = pesel;
        this.addresses = addresses;
        this.reservationDtos=new ArrayList<>();
    }

    //TODO: dodac metode do dodawania rezerwacji lub w konstruktorze
}
