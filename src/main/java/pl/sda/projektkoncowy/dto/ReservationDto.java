package pl.sda.projektkoncowy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.sda.projektkoncowy.entity.Car;
import pl.sda.projektkoncowy.entity.Rental;
import pl.sda.projektkoncowy.entity.Return;

import javax.persistence.CascadeType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
public class ReservationDto {

    private UUID idReservation;
    private LocalDateTime dateReservation;

    //private CarDto carDto;
    private UUID carId;

    private LocalDateTime rentalFrom;
    private LocalDateTime rentalTo;
    private UUID idBranchRental;
    private UUID idBranchReturn;
    private BigDecimal cost;

    private RentalDto rentalCarDto;

    private ReturnDto returnCarDto;
    private UUID customerId;

    public ReservationDto(LocalDateTime dateReservation, UUID carId, LocalDateTime rentalFrom, LocalDateTime rentalTo, UUID idBranchRental, UUID idBranchReturn, BigDecimal cost, RentalDto rentalCarDto, ReturnDto returnCarDto, UUID customerId) {
        this.dateReservation = dateReservation;
        this.carId = carId;
        this.rentalFrom = rentalFrom;
        this.rentalTo = rentalTo;
        this.idBranchRental = idBranchRental;
        this.idBranchReturn = idBranchReturn;
        this.cost = cost;
        this.rentalCarDto = rentalCarDto;
        this.returnCarDto = returnCarDto;
        this.customerId=customerId;
    }



    public ReservationDto(UUID idReservation, LocalDateTime dateReservation, UUID carId, LocalDateTime rentalFrom, LocalDateTime rentalTo, UUID idBranchRental, UUID idBranchReturn, BigDecimal cost, RentalDto rentalCarDto, ReturnDto returnCarDto, UUID customerId) {
        this.idReservation = idReservation;
        this.dateReservation = dateReservation;
        this.carId = carId;
        this.rentalFrom = rentalFrom;
        this.rentalTo = rentalTo;
        this.idBranchRental = idBranchRental;
        this.idBranchReturn = idBranchReturn;
        this.cost = cost;
        this.rentalCarDto = rentalCarDto;
        this.returnCarDto = returnCarDto;
        this.customerId=customerId;
    }

    public ReservationDto(UUID idReservation, LocalDateTime dateReservation, UUID carId, LocalDateTime rentalFrom, LocalDateTime rentalTo, UUID idBranchRental, UUID idBranchReturn, BigDecimal cost,  UUID customerId) {
        this.idReservation = idReservation;
        this.dateReservation = dateReservation;
        this.carId = carId;
        this.rentalFrom = rentalFrom;
        this.rentalTo = rentalTo;
        this.idBranchRental = idBranchRental;
        this.idBranchReturn = idBranchReturn;
        this.cost = cost;
        this.customerId=customerId;
    }
}
