package pl.sda.projektkoncowy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class AvailableCarsDto {

    private LocalDate rentalFrom;
    //private String rentalCity;

    public AvailableCarsDto(LocalDate rentalFrom) {
        this.rentalFrom = rentalFrom;
    }
}
