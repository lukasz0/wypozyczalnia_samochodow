package pl.sda.projektkoncowy.restController;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.sda.projektkoncowy.dto.CarDto;
import pl.sda.projektkoncowy.dto.ReservationDto;
import pl.sda.projektkoncowy.entity.Branch;
import pl.sda.projektkoncowy.service.BranchService;
import pl.sda.projektkoncowy.service.CarService;
import pl.sda.projektkoncowy.service.ReservationService;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("api/reservations")
public class ReservationController {

    private final ReservationService reservationService;
    private final pl.sda.projektkoncowy.service.CarService carService;
    private final BranchService branchService;

    public ReservationController(ReservationService reservationService, CarService carService, BranchService branchService){
        this.reservationService= Objects.requireNonNull(reservationService);
        this.carService= Objects.requireNonNull(carService);
        this.branchService= Objects.requireNonNull(branchService);
    }

    @GetMapping
    @Secured("ROLE_Admin")
    ResponseEntity<List<ReservationDto>> getReservations(){
        return ResponseEntity.ok(reservationService.getReservations());
    }

    @GetMapping(value = "/id/{id}")
    @Secured("ROLE_Admin")
    ResponseEntity<ReservationDto> getById(@PathVariable UUID id){
        ReservationDto reservationDto=reservationService.getById(id);
        return ResponseEntity.ok(reservationDto);
    }

    @PostMapping
    @Secured("ROLE_Admin")
    ResponseEntity<ReservationDto> addReservation(@RequestBody ReservationDto reservationDto){
        CarDto carDto = carService.getById(reservationDto.getCarId());
        Branch branch= branchService.getById(carDto.getIdBranch());
        ReservationDto reservationDtoOut=reservationService.addReservation(reservationDto, carDto, branch);
        return ResponseEntity.status(HttpStatus.CREATED).body(reservationDtoOut);
    }
    @GetMapping(value="/car/{id}")
    ResponseEntity<List<ReservationDto>> getByCarId(@PathVariable UUID id){
        List<ReservationDto> reservationDtoList = reservationService.getByCarId(id);
        return ResponseEntity.ok(reservationDtoList);
    }

    @GetMapping(value="/customer/{id}")
    ResponseEntity<List<ReservationDto>> getByCustomerId(@PathVariable UUID id){
        List<ReservationDto> reservationDtos=reservationService.getByCustomerId(id);
        return ResponseEntity.ok(reservationDtos);
    }
}
