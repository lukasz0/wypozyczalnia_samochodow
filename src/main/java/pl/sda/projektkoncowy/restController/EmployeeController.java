package pl.sda.projektkoncowy.restController;

import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.sda.projektkoncowy.dto.BranchId;
import pl.sda.projektkoncowy.dto.EmployeeDto;
import pl.sda.projektkoncowy.entity.Address;
import pl.sda.projektkoncowy.entity.Branch;
import pl.sda.projektkoncowy.entity.Employee;
import pl.sda.projektkoncowy.service.AddressService;
import pl.sda.projektkoncowy.service.BranchService;
import pl.sda.projektkoncowy.service.EmployeeService;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {
    private EmployeeService employeeService;
    private final AddressService addressService;
    private final BranchService branchService;

    public EmployeeController(EmployeeService employeeService, AddressService addressService, BranchService branchService) {
        this.employeeService = Objects.requireNonNull(employeeService);
        this.addressService = Objects.requireNonNull(addressService);
        this.branchService = Objects.requireNonNull(branchService);
    }

    @GetMapping
    //@Secured("ROLE_Admin")
    ResponseEntity<List<EmployeeDto>> getEmployees(){
        return ResponseEntity.ok(employeeService.getEmployees());
    }

    @PostMapping
    //@Secured("ROLE_Admin")
    ResponseEntity<EmployeeDto> addEmployee(@RequestBody EmployeeDto employeeDto){
        Address address = addressService.addAddress(employeeDto.getRegisterBranchForm().getAddressDto());
        BranchId branchId = branchService.addBranch(address);
        Branch branch=branchService.getById(branchId.getId());

        return ResponseEntity.status(HttpStatus.CREATED).body(employeeService.addEmployee(employeeDto,branch));
    }

    @DeleteMapping(value="/{id}")
    //@Secured("ROLE_Admin")
    ResponseEntity<Void> deleteEmployeeById(@PathVariable UUID id){
        employeeService.deleteEmployeeById(id);
        return ResponseEntity.noContent().build();
    }

}
