package pl.sda.projektkoncowy.restController;

import lombok.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.sda.projektkoncowy.exception.CustomerNotExistsException;

import java.time.Instant;

@RestControllerAdvice
public class ExceptionHandler {

    @Value
    static class ErrorMessage{
        private String message;
        Instant errorTime;
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(CustomerNotExistsException.class)
    public ResponseEntity<ErrorMessage> handler(CustomerNotExistsException ex){
        return ResponseEntity.badRequest().body(new ErrorMessage(ex.getMessage(),Instant.now()));
    }
}
