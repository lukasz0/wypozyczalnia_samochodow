package pl.sda.projektkoncowy.restController;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.sda.projektkoncowy.dto.CarRentalDto;
import pl.sda.projektkoncowy.dto.CustomerDto;
import pl.sda.projektkoncowy.dto.RegisterBranchForm;
import pl.sda.projektkoncowy.entity.Branch;
import pl.sda.projektkoncowy.entity.CarRental;
import pl.sda.projektkoncowy.service.CarRentalService;

import java.util.Objects;
import java.util.UUID;

import static pl.sda.projektkoncowy.service.BranchService.mapperToRegisterBranchFromBranchService;
import static pl.sda.projektkoncowy.service.CarRentalService.mapperToCarRentalDto;

@RestController
@RequestMapping("api/car_rentals")
public class CarRentalRestController {
    private final CarRentalService carRentalService;

    public CarRentalRestController(CarRentalService carRentalService) {
        this.carRentalService = Objects.requireNonNull(carRentalService);
    }

    @GetMapping(value = "/id/{id}")
    @Secured("ROLE_Admin")
    ResponseEntity<CarRentalDto> getById(@PathVariable UUID id) {
        CarRental carRental = carRentalService.getById(id);
        CarRentalDto carRentalDto = mapperToCarRentalDto(carRental);
        return ResponseEntity.ok(carRentalDto);
    }

    @PostMapping
    @Secured("ROLE_Admin")
    ResponseEntity<CarRental> addCarRental(@RequestBody CarRental carRentalSave) {
        CarRental carRental = carRentalService.addCarRentalV2(carRentalSave);
        return ResponseEntity.status(HttpStatus.CREATED).body(carRental);
    }

    //Usueniecie za pomoca id
    @DeleteMapping(value = "/{id}")
    @Secured("ROLE_Admin")
    ResponseEntity<Void> deleteCarRentalsV2(@PathVariable UUID id) {
        carRentalService.deleteCarRentalV2(id);
        return ResponseEntity.noContent().build();
    }

    //Usuniecie calego carRentals... Nie wiem czy taki sposob jest w ogole wlasciwy
    @DeleteMapping
    @Secured("ROLE_Admin")
    ResponseEntity<Void> deleteCarRentals() {
        carRentalService.deleteCarRental();
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}")
    @Secured("ROLE_Admin")
    ResponseEntity<Void> updateCarRentals(@PathVariable UUID id, @RequestBody CarRentalDto carRentalDto) {
        carRentalService.updateCarRentals(id, carRentalDto);
        return ResponseEntity.noContent().build();
    }
}
