package pl.sda.projektkoncowy.restController;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.sda.projektkoncowy.dto.RentalDto;
import pl.sda.projektkoncowy.service.RentalService;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("api/rentals")
public class RentalController {

    private final RentalService rentalService;

    public RentalController(RentalService rentalService) {
        this.rentalService = rentalService;
    }

    @GetMapping
    @Secured("ROLE_Admin")
    ResponseEntity<List<RentalDto>> getAll(){
        List<RentalDto> rentalDtos=rentalService.getAll();
        return ResponseEntity.ok(rentalDtos);
    }

    @PostMapping
    @Secured("ROLE_Admin")
    ResponseEntity<RentalDto> addRental(@RequestBody RentalDto rentalDto){
        RentalDto rentalDtoOut = rentalService.addRental(rentalDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(rentalDtoOut);
    }

}
