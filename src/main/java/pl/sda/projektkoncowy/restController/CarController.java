package pl.sda.projektkoncowy.restController;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.sda.projektkoncowy.dto.AvailableCarsDto;
import pl.sda.projektkoncowy.dto.CarDto;
import pl.sda.projektkoncowy.entity.Branch;
import pl.sda.projektkoncowy.service.BranchService;
import pl.sda.projektkoncowy.service.CarService;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("api/cars")
public class CarController {

    private final CarService carService;
    private final BranchService branchService;

    public CarController(CarService carService,BranchService branchService) {
        this.carService = Objects.requireNonNull( carService);
        this.branchService = Objects.requireNonNull( branchService);
    }

    @GetMapping
    @Secured({"ROLE_Admin", "ROLE_User"})
    ResponseEntity<List<CarDto>> getCars(){
        List<CarDto> cars=carService.getCars();
        return ResponseEntity.ok(cars);
    }

    @GetMapping(value = "/id/{id}")
    @Secured("ROLE_Admin")
    ResponseEntity<CarDto> getById(@PathVariable UUID id){
        CarDto carDto=carService.getById(id);
        return ResponseEntity.ok(carDto);
    }

    @PostMapping
    @Secured("ROLE_Admin")
    ResponseEntity<CarDto> addCar(@RequestBody CarDto carDto){
        Branch branch= branchService.getById(carDto.getIdBranch());
        CarDto carDto1=carService.addCar(carDto, branch);
        return ResponseEntity.status(HttpStatus.CREATED).body(carDto1);
    }

    //TODO: a jak chciałbym dwa przekazać dwie wartości to wtefy tak
//    @GetMapping(value="/{localDate}/{city}")
//    ResponseEntity<List<CarDto>> availableCars(@PathVariable String localDate,@PathVariable String city )
    @GetMapping(value="/{localDate}")
    ResponseEntity<List<CarDto>> availableCars(@PathVariable String localDate){
        AvailableCarsDto availableCarsDto = new AvailableCarsDto( LocalDate.parse(localDate));
        List<CarDto> carDtoList = carService.getAvailableCars(availableCarsDto);
        return ResponseEntity.ok(carDtoList);
    }
}
