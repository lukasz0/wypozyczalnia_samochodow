package pl.sda.projektkoncowy.restController;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.sda.projektkoncowy.dto.AddressDto;
import pl.sda.projektkoncowy.dto.BranchId;
import pl.sda.projektkoncowy.dto.RegisterBranchForm;
import pl.sda.projektkoncowy.entity.Address;
import pl.sda.projektkoncowy.service.AddressService;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/addresses")
public class AddressRestController {
    private final AddressService addressService;
    public AddressRestController(AddressService addressService){
        this.addressService= Objects.requireNonNull(addressService);
    }

//    @GetMapping
//    String test(){
//        return "test";
//    }
    @PostMapping
    @Secured("ROLE_Admin")
    ResponseEntity<Address> addAddress(@RequestBody AddressDto addressDto){
        Address address= addressService.addAddress(addressDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(address);
    }

    @GetMapping
    @Secured("ROLE_Admin")
    ResponseEntity<List<AddressDto>> getAddresses(){
        List<AddressDto> addressList=addressService.getListAddresses();
        return ResponseEntity.ok(addressList);
    }
    @PutMapping("/{id}")
    @Secured("ROLE_Admin")
    ResponseEntity<Void> updateAddress(@PathVariable UUID id, @RequestBody AddressDto addressDto){
        addressService.updateAddress(id, addressDto);
        return ResponseEntity.noContent().build();
    }
}
