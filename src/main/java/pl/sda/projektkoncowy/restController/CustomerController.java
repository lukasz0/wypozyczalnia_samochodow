package pl.sda.projektkoncowy.restController;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.sda.projektkoncowy.dto.CustomerDto;
import pl.sda.projektkoncowy.dto.RegisterCompanyForm;
import pl.sda.projektkoncowy.dto.RegisterPersonForm;
import pl.sda.projektkoncowy.entity.Address;
import pl.sda.projektkoncowy.service.AddressService;
import pl.sda.projektkoncowy.service.CustomerService;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("/api/customers")
public class CustomerController {

    private final CustomerService customerService;
    private final AddressService addressService;

    CustomerController(CustomerService customerService, AddressService addressService){
        this.customerService= Objects.requireNonNull(customerService);
        this.addressService=Objects.requireNonNull(addressService);
    }

    @GetMapping
    @Secured("ROLE_Admin")
    ResponseEntity<List<CustomerDto>> getListOfCustomers(){
        return ResponseEntity.ok(customerService.listAllCustomers());
    }

    @GetMapping("/{customerId}")
    @Secured("ROLE_Admin")
    ResponseEntity<CustomerDto> findById(@PathVariable UUID customerId){
        return ResponseEntity.ok(customerService.findById(customerId));
    }

    @PostMapping("/company")
    @Secured("ROLE_Admin")
    ResponseEntity<CustomerDto> registerCompany(@RequestBody RegisterCompanyForm form){
        Address address=addressService.addAddress(form.getAddressDto());
        CustomerDto customerDto = customerService.registerCompany(form,address);
        return ResponseEntity.status(HttpStatus.CREATED).body(customerDto);
    }
    @PostMapping("/person")
    @Secured("ROLE_Admin")
    ResponseEntity<CustomerDto> registerPerson(@RequestBody RegisterPersonForm form){
        Address address=addressService.addAddress(form.getAddressDto());
        CustomerDto customerDto =customerService.registerPerson(form,address);
        return ResponseEntity.status(HttpStatus.CREATED).body(customerDto);
    }
//    @DeleteMapping
//    ResponseEntity<Void> deleteById
}
