package pl.sda.projektkoncowy.restController;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.sda.projektkoncowy.dto.ReturnDto;
import pl.sda.projektkoncowy.service.ReturnService;

import java.util.List;

@RestController
@RequestMapping("api/returns")
public class ReturnController {
    public final ReturnService returnService;

    public ReturnController(ReturnService returnService) {
        this.returnService = returnService;
    }

    @GetMapping
    @Secured("ROLE_Admin")
    ResponseEntity<List<ReturnDto>> getAll(){
        List<ReturnDto> returnDtos = returnService.getAll();
        return ResponseEntity.ok(returnDtos);
    }

    @PostMapping
    @Secured("ROLE_Admin")
    ResponseEntity<ReturnDto> addReturn(@RequestBody ReturnDto returnDto){
        ReturnDto returnDtoOut=returnService.addReturn(returnDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(returnDtoOut);
    }

}
