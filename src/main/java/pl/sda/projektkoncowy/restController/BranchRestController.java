package pl.sda.projektkoncowy.restController;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.sda.projektkoncowy.dto.BranchId;
import pl.sda.projektkoncowy.dto.RegisterBranchForm;
import pl.sda.projektkoncowy.entity.Address;
import pl.sda.projektkoncowy.entity.Branch;
import pl.sda.projektkoncowy.mapper.CarMapper;
import pl.sda.projektkoncowy.service.AddressService;
import pl.sda.projektkoncowy.service.BranchService;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static pl.sda.projektkoncowy.service.BranchService.mapperToRegisterBranchFromBranchService;

@RestController
@RequestMapping("api/branches")
public class BranchRestController {

    private final BranchService branchService;
    private final AddressService addressService;


    public BranchRestController (BranchService branchService, AddressService addressService){
        this.branchService = Objects.requireNonNull(branchService);
        this.addressService=Objects.requireNonNull(addressService);
    }

    @GetMapping
    @Secured("ROLE_Admin")
    ResponseEntity<List<RegisterBranchForm>> getBranches(){
        List<RegisterBranchForm>  branchForms = branchService.getBranches();
        return ResponseEntity.ok(branchForms);
    }

    @PostMapping
    @Secured("ROLE_Admin")
    ResponseEntity<BranchId> addBranch(@RequestBody RegisterBranchForm form){
        Address address = addressService.addAddress(form);
        BranchId branchId = branchService.addBranch(address);
        return ResponseEntity.status(HttpStatus.CREATED).body(branchId);
    }

    @DeleteMapping(value ="/{id}")
    @Secured("ROLE_Admin")
    ResponseEntity<Void> deleteBranch(@PathVariable UUID id){
        branchService.deleteBranch(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/id/{id}")
    @Secured("ROLE_Admin")
    ResponseEntity<RegisterBranchForm> getById(@PathVariable UUID id){
        Branch branch = branchService.getById(id);
        RegisterBranchForm form = mapperToRegisterBranchFromBranchService(branch);
        return ResponseEntity.ok(form);
    }

    @GetMapping(value="/{city}")
    @Secured("ROLE_Admin")
    ResponseEntity<List<RegisterBranchForm>> getByCity(@PathVariable String city){
        List<RegisterBranchForm> formList=branchService.getByCity(city);
        return ResponseEntity.ok(formList);
    }

}
