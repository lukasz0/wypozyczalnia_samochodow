package pl.sda.projektkoncowy.exception;

public class CustomerNotExistsException extends BuisnessServiceException {

    public CustomerNotExistsException(String message) {
        super(message);
    }

    public CustomerNotExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
