package pl.sda.projektkoncowy.exception;

public class EmailAlreadyExistsException  extends BuisnessServiceException{

    public EmailAlreadyExistsException(String message) {
        super(message);
    }

    public EmailAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
