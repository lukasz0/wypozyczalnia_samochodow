package pl.sda.projektkoncowy.exception;

public class PeselAlreadyExistsException extends BuisnessServiceException {
    public PeselAlreadyExistsException(String message,Throwable cause) {
        super(message,cause);
    }
    public PeselAlreadyExistsException(String message){
        super(message);
    }
}
