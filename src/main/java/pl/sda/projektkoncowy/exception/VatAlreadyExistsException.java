package pl.sda.projektkoncowy.exception;

public class VatAlreadyExistsException extends BuisnessServiceException {

    public VatAlreadyExistsException(String message) {
        super(message);
    }

    public VatAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
