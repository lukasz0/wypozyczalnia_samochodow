package pl.sda.projektkoncowy.exception;

public abstract class BuisnessServiceException extends  RuntimeException{

    public BuisnessServiceException(String message) {
        super(message);
    }

    public BuisnessServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
