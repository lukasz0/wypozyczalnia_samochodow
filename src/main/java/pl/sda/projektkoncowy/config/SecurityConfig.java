package pl.sda.projektkoncowy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
//Adnotacja, która pozwala na ustalenie ról do poszczególnych endpointów.
//Np. Dodanie adnotacji "@Secured("ROLE_admin")" nad metodą POST lub DELETE
//żeby użytkownik nie miał do nich dostępu
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    /*@Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/api/employee").hasRole("Admin")
                .antMatchers("/api/**").authenticated()
                .antMatchers("public/**").permitAll()
                .antMatchers("/h2/**").hasRole("Admin")
                .and()
                .formLogin()
                .permitAll()
                .and()
                .logout()
                .and()
                .csrf().disable()
                .httpBasic();
    }

    //uzytkownicy w pamieci. Istnieje mozliwosc dodania do bazy danych
    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("test").password(passwordEncoder().encode("test")).roles("Admin")
                .and()
                .withUser("user2").password(passwordEncoder().encode("user2")).roles("User");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }*/

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .mvcMatchers("/api/**").authenticated()
                .mvcMatchers("/api/addresses").hasRole("Admin")
                .mvcMatchers("/api/branches").hasRole("Admin")
                .mvcMatchers("/api/car_rentals").hasRole("Admin")
                .mvcMatchers("/api/COMPANY").hasRole("Admin")
                .mvcMatchers("/api/customers").hasRole("Admin")
                .mvcMatchers("/api/employees").hasRole("Admin")
                .mvcMatchers("/api/PERSON").hasRole("Admin")
                .mvcMatchers("/api/reservations").hasRole("Admin")
                .mvcMatchers("/api/cars").permitAll()
                //Przeniosłem "/api/**".authenticated() na górę ponieważ jak była
                //na dole, to do "cars" przy PermitAll() można było wejść bez logowania
                .mvcMatchers(HttpMethod.GET, "/api/cars").hasRole("User")
                //Dzięki tej metodzie, User ma dostęp jedynie do metody GET w cars,
                //a nie do wszystkich metod
                .and()
                .formLogin()
                .permitAll()
                .and()
                .logout()
                .and()
                .csrf().disable()
                .httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin").password(passwordEncoder().encode("admin")).roles("Admin")
                .and()
                .withUser("user").password(passwordEncoder().encode("user")).roles("User");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
