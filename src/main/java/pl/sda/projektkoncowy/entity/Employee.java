package pl.sda.projektkoncowy.entity;

import lombok.Data;
import pl.sda.projektkoncowy.enumarated.JobTitle;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "employees")
@Data
public class Employee {
    @Id
    private UUID employee_id;
    private String firstName;
    private String lastName;
    @Enumerated(EnumType.STRING)
    private JobTitle jobTitle;
    private String password;
    @ManyToOne(cascade = CascadeType.ALL)
    private Branch branch;


    private Employee(){};

    public Employee(String firstName, String lastName, JobTitle jobTitle, String password, Branch branch) {
        this.employee_id = UUID.randomUUID();
        this.firstName = Objects.requireNonNull(firstName);
        this.lastName = Objects.requireNonNull(lastName);
        this.jobTitle = Objects.requireNonNull(jobTitle);
        this.password = Objects.requireNonNull(password);
        this.branch = Objects.requireNonNull(branch);
    }
}
