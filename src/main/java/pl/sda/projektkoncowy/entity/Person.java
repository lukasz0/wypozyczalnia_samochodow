package pl.sda.projektkoncowy.entity;

import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Objects;

@Entity
@Data
@DiscriminatorValue("PERSON")
public class Person extends Customer {

    private String firstName;
    private String lastName;
    private String pesel;

    protected Person(){};

    public Person(String email,Address address,String password, String firstName, String lastName, String pesel) {
        super(email,password,address );
        this.firstName = Objects.requireNonNull(firstName);
        this.lastName = Objects.requireNonNull(lastName);
        this.pesel = Objects.requireNonNull(pesel);
    }
}
