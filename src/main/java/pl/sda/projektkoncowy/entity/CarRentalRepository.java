package pl.sda.projektkoncowy.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

public interface CarRentalRepository extends JpaRepository<CarRental, UUID> {

    //Trzeba stworzyć getCarRentals poprzez mapowanie
    @Query(value = "select * from car_rentals", nativeQuery = true)
    CarRental getCarRentals();

    @Query(value = "SELECT * FROM car_rentals as b WHERE b.id=?1", nativeQuery = true)
    CarRental getCarRentalsById(UUID id);

    //Usuniecie calej tabeli "car_rentas"
    @Query(value = "delete from car_rentals", nativeQuery = true)
    void delete();

    //Usueniecie "car_rentals" przy pomocy "id"... nie wiem ktory sposob jest wlasciwy
    @Query(value = "DELETE FROM car_rentals as cr WHERE cr.id=?1", nativeQuery = true)
    void deleteById();
}
