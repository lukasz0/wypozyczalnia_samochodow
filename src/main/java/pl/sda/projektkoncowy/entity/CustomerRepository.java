package pl.sda.projektkoncowy.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface CustomerRepository extends JpaRepository<Customer, UUID> {

    List<Person> findAllByFirstNameIgnoreCaseLikeAndLastNameIgnoreCaseLike(String firstName, String lastName);

    @Query("select p from Person p where upper(p.firstName) like upper(?1) and upper(p.lastName) like upper(?2)")// ?1 - oznacza pierwszy parametr metody. Zapytanie JPAQL lub HQL (poruszamy się po klasach i polach a nie po tablech i kolumnach)
    List<Person> filterByPersonName(String firstName, String lastName);


    List<Customer> findAllByEmailContains(String email);


    List<Customer> findAllByAddressesCityIgnoreCase(String city);


    @Query("from Company c join c.addresses a where c.name like %?1% and a.country=?2")
    List<Company> findCompainesInCountry(String partOfName, String country);

    @Query("select distinct a.street from Person p join p.addresses a where upper(p.lastName) like upper(?1) order by a.street")
    List<String> findStreetsForLastName(String lastName);

    @Query(value = "select p.first_name, p.last_name, a.city "
            + "from customers p "
            + "inner join customer_addresses a on a.customer_id = p.id "
            + "where p.customer_type = 'PERSON' and a.country = ?1 order by p.last_name, a.city",
            nativeQuery = true)
    List<Object[]> findPersonNameByCountry(String country);

    @Query("select (count(c.id)>0) from Customer c where upper(c.email) = upper(?1)")
    boolean hasCustomerWithEmail(String email);

    @Query("select (count(c.id)>0) from Company c where upper(c.vat) = upper(?1)")
    boolean hasCompanyWithVat(String vat);

}
