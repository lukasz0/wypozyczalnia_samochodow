package pl.sda.projektkoncowy.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
public class Return {

    @Id
    private UUID returnId;
    private LocalDateTime returnDate;
    @OneToOne(cascade = CascadeType.PERSIST)
    private Reservation reservation;
    private BigDecimal surcharge;

    public Return() {
    }

    public Return(LocalDateTime returnDate, Reservation reservation, BigDecimal surcharge) {
        this.returnId = UUID.randomUUID();
        this.returnDate = returnDate;
        this.reservation = reservation;
        this.surcharge = surcharge;
    }
}
