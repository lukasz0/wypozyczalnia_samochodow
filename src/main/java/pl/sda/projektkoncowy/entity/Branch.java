package pl.sda.projektkoncowy.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

//lombok
@Entity
@Table(name = "branches")
@Data
@AllArgsConstructor
public final class Branch {
    @Id
    private UUID id;
    @ManyToOne
    private Address address;


    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="branch_id")
    private List<Employee> employeeList;


    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name="branch_id")
    private List<Car> carList;

    //constructor only for JPA
    private Branch() {
    };

    public Branch(Address address) {
        this.id = UUID.randomUUID();
        this.address = Objects.requireNonNull(address);
        this.employeeList = new ArrayList<>();
        this.carList = new ArrayList<>();
    }



    public void addEmployee(Employee employee){
        if(!employeeList.contains(employee)){
            employeeList.add(employee);
        }
    }

    public Branch(Address address, List<Employee> employeeList, List<Car> carList) {
        this.address = address;
        this.employeeList = employeeList;
        this.carList = carList;
    }

    public void addCar(Car car){
        if(!carList.contains(car)){
            carList.add(car);
        }
    }
}
