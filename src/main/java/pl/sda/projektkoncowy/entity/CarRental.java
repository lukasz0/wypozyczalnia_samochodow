package pl.sda.projektkoncowy.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "car_rentals")
@Data
public final class CarRental {

    @Id
    private UUID id;

    private String name;
    private String website;
    private String phoneNumber;
    private String owner;
    private String logoType;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "carRental_id")
    private List<Branch> branchList;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "carRental_id")
    private List<Car> carList;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "carRental_id")
    private List<Employee> employeeList;

    public CarRental() {
    }

    //Dodanie konstrukora z "name" w celu stworzenia CarRental z parametrem "(String name)" w CarRentalService
    public CarRental(String name) {
        this.name = name;
    }

    //Konstruktor roboczy dodany w celu stworzenia metody updateCarRentals, która przyjmuje tylko strini.
    //Na razie bez list
//    public CarRental(String name, String website, String phoneNumber, String owner, String logoType) {
//        this.name = name;
//        this.website = website;
//        this.phoneNumber = phoneNumber;
//        this.owner = owner;
//        this.logoType = logoType;
//    }


    public CarRental(UUID id, String name, String website, String phoneNumber, String owner, String logoType) {
        this.id = id;
        this.name = name;
        this.website = website;
        this.phoneNumber = phoneNumber;
        this.owner = owner;
        this.logoType = logoType;
    }

    public CarRental(String name, String website, String phoneNumber, String owner,
                     String logoType, List<Branch> branchList, List<Car> carList, List<Employee> employeeList) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.website = website;
        this.phoneNumber = phoneNumber;
        this.owner = owner;
        this.logoType = logoType;
        this.branchList = branchList;
        this.carList = carList;
        this.employeeList = employeeList;
    }
}
