package pl.sda.projektkoncowy.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface BranchRepository extends JpaRepository<Branch, UUID> {

    @Query(value = "select * "
            + "from branches",
            nativeQuery = true)
    List<Branch> getBranches();

    @Query(value="SELECT * FROM branches as b WHERE b.id=?1", nativeQuery = true)
    Branch getBranchById(UUID id);

    @Query(value = "SELECT * FROM branches b JOIN addresses a ON b.address_id=a.id WHERE a.city=?1", nativeQuery = true)
    List<Branch>
    getByCity(String city);
}
