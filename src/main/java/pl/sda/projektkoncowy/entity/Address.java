package pl.sda.projektkoncowy.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name="addresses")
@Data
public class Address {

    @Id
    private UUID id;
    private String street;
    private String city;
    private String zipCode;
    private String country;


    protected Address() {};

    public Address(String street, String city, String zipCode, String country) {
        this.id=UUID.randomUUID();
        this.street = Objects.requireNonNull(street);
        this.city = Objects.requireNonNull(city);
        this.zipCode = Objects.requireNonNull(zipCode);
        this.country = Objects.requireNonNull(country);
    }

    public Address(UUID id, String street, String city, String zipCode, String country) {
        this.id = id;
        this.street = street;
        this.city = city;
        this.zipCode = zipCode;
        this.country = country;
    }
}
