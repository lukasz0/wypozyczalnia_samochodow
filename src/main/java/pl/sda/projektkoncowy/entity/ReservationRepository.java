package pl.sda.projektkoncowy.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.projektkoncowy.dto.ReservationDto;

import java.util.List;
import java.util.UUID;

public interface ReservationRepository extends JpaRepository<Reservation, UUID> {


    List<Reservation> getByCar(Car car);

    List<Reservation> getByCustomer(Customer customer);
}
