package pl.sda.projektkoncowy.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table(name="reservations")
public class Reservation {

    @Id
    private UUID idReservation;
    private LocalDateTime dateReservation;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private Car car;
    private LocalDateTime rentalFrom;
    private LocalDateTime rentalTo;
    private UUID idBranchRental;
    private UUID idBranchReturn;
    private BigDecimal cost;

    @OneToOne(cascade = CascadeType.ALL)
    private Rental rentalCar;

    @OneToOne(cascade = CascadeType.ALL)
    private Return returnCar;

    @ManyToOne
    private Customer customer;

    protected Reservation(){};

    public Reservation(LocalDateTime dateReservation, Car car, LocalDateTime rentalFrom, LocalDateTime rentalTo,UUID idBranchRental, UUID idBranchReturn, BigDecimal cost, Customer customer) {
        this.idReservation = UUID.randomUUID();
        this.dateReservation = dateReservation;
        this.car = car;
        this.rentalFrom = rentalFrom;
        this.rentalTo = rentalTo;
        this.idBranchRental = idBranchRental;
        this.idBranchReturn = idBranchReturn;
        this.cost = cost;
        this.customer=customer;
    }

    public Reservation(UUID idReservation, LocalDateTime dateReservation, Car car, LocalDateTime rentalFrom, LocalDateTime rentalTo, UUID idBranchRental, UUID idBranchReturn, BigDecimal cost, Customer customer) {
        this.idReservation = idReservation;
        this.dateReservation = dateReservation;
        this.car = car;
        this.rentalFrom = rentalFrom;
        this.rentalTo = rentalTo;
        this.idBranchRental = idBranchRental;
        this.idBranchReturn = idBranchReturn;
        this.cost = cost;
        this.customer = customer;
    }
}
