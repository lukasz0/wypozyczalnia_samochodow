package pl.sda.projektkoncowy.entity;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
public class Rental {

    @Id
    private UUID idRental;
    private LocalDateTime rentalDate;
    @OneToOne(cascade = CascadeType.ALL)
    private Reservation reservation;
    private String comments;

    public Rental() {
    }

    public Rental(LocalDateTime rentalDate, Reservation reservation, String comments) {
        this.idRental = UUID.randomUUID();
        this.rentalDate = rentalDate;
        this.reservation = reservation;
        this.comments = comments;
    }
}
