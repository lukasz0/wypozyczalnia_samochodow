package pl.sda.projektkoncowy.entity;

import lombok.*;
import pl.sda.projektkoncowy.enumarated.Category;
import pl.sda.projektkoncowy.enumarated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "cars")
@Data
public class Car {

    @Id
    private UUID id;

    private String carBrand;
    private String model;
    private String bodyType;
    private String year;
    private String color;
    private String mileage;
    private Long price;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="car_id")
    private List<Reservation> reservationList;

    //TODO: czy tu powinno być refresh
    @ManyToOne(cascade = CascadeType.REFRESH)
    private Branch branch;


    @Enumerated(EnumType.STRING)
    private Status status;
    @Enumerated(EnumType.STRING)
    private Category category;

    public Car() {
    }

    public Car(String carBrand, String model, String bodyType, String year, String color, String mileage, Long price, List<Reservation> reservationList, Branch branch, Status status, Category category) {

        this.id=UUID.randomUUID();
        this.carBrand = carBrand;
        this.model = model;
        this.bodyType = bodyType;
        this.year = year;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
        this.reservationList = reservationList;
        this.branch = branch;
        this.status = status;
        this.category = category;
    }

    public Car(String carBrand, String model, String bodyType, String year, String color, String mileage, Long price, Branch branch, Status status, Category category) {

        this.id=UUID.randomUUID();
        this.carBrand = carBrand;
        this.model = model;
        this.bodyType = bodyType;
        this.year = year;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
        this.reservationList = new ArrayList<>();
        this.branch = branch;
        this.status = status;
        this.category = category;
    }

    public Car(UUID id, String carBrand, String model, String bodyType, String year, String color, String mileage, Long price, List<Reservation> reservationList, Branch branch, Status status, Category category) {
        this.id = id;
        this.carBrand = carBrand;
        this.model = model;
        this.bodyType = bodyType;
        this.year = year;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
        this.reservationList = reservationList;
        this.branch = branch;
        this.status = status;
        this.category = category;
    }
}
