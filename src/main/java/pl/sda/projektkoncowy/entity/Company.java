package pl.sda.projektkoncowy.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import static java.util.Objects.requireNonNull;

@Entity
@DiscriminatorValue("COMPANY")
@Data
public class Company extends Customer{
    private String name;

    @Column(unique = true)
    private String vat;

    protected Company() {
    }

    public Company(String email, Address address, String password, String name, String vat) {
        super(email, password,address);
        this.name = requireNonNull(name);
        this.vat = requireNonNull(vat);
    }
    public Company(String email, Address address, String name, String vat) {
        super(email, address);
        this.name = requireNonNull(name);
        this.vat = requireNonNull(vat);
    }

}
