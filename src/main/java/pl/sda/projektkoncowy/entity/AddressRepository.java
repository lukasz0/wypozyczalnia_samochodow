package pl.sda.projektkoncowy.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface AddressRepository extends JpaRepository<Address, UUID> {

    @Query("from Address")
    List<Address> getListAddresses();

    @Query(value = "update addresses  set street = ?1, city = ?2, zip_Code = ?3, country = ?4 where id = ?5 ", nativeQuery = true)
    void updateListAddresses(String street, String city, String zipCode, String country, UUID id);

}
