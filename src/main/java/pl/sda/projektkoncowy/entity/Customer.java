package pl.sda.projektkoncowy.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.Objects.requireNonNull;

@Entity
@Table(name = "customers")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "customer_type")
@Data
public abstract class Customer {
    @Id
    private UUID id;

    @Column(unique = true)
    private String email;

    @ManyToOne//(fetch = FetchType.EAGER, cascade =CascadeType.MERGE)
    //@JoinColumn(name = "customer_id")
    private Address addresses;
    private String password;

    @OneToMany
    @JoinColumn(name ="customer_id")
    private List<Reservation> reservations;

    protected Customer() {
    }

    public Customer(String email, String password, Address address) {
        this.id = UUID.randomUUID();
        this.email = requireNonNull(email);
        this.addresses = address;
        this.password=password;
        this.reservations = new ArrayList<>();
    }

    public Customer(String email,Address address) {
        this.id = UUID.randomUUID();
        this.email = requireNonNull(email);
        this.addresses = address;
        this.reservations = new ArrayList<>();
    }

    //TODO: dodac metode do dodawania rezerwacji lub w konstruktorze

}
