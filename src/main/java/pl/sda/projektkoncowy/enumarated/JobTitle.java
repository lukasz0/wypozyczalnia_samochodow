package pl.sda.projektkoncowy.enumarated;

public enum JobTitle {
    chairman,
    manager,
    employee
}
