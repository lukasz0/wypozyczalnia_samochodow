package pl.sda.projektkoncowy.enumarated;

public enum Category {
    SEDAN,
    COMBI,
    SUV,
    CABRIOLET,
    TRUCK
}
