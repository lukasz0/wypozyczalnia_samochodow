package pl.sda.projektkoncowy.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.projektkoncowy.dto.AddressDto;
import pl.sda.projektkoncowy.dto.CarRentalDto;
import pl.sda.projektkoncowy.entity.Address;
import pl.sda.projektkoncowy.entity.CarRental;
import pl.sda.projektkoncowy.entity.CarRentalRepository;

import java.util.Objects;
import java.util.UUID;

@Service
public class CarRentalService {
    private final CarRentalRepository carRentalRepository;

    public CarRentalService(CarRentalRepository carRentalRepository) {
        this.carRentalRepository = Objects.requireNonNull(carRentalRepository);
    }

    //Stworzenie metody dodajacej CarRentals poprzez klase CarRental, a nie przez klase DTO, poniewaz w DTO
    //są pola typu komenatrze, rezerwacje itp. Wydaje mi sie, ze lepiej dodac poprzez "name"
    /*@Transactional
    public CarRental addCarRentals(String name) {
        CarRental carRental = new CarRental(name);
        carRentalRepository.save(carRental);
        return new CarRental(carRental.getName());
    }*/

    @Transactional
    public CarRental addCarRentalV2(CarRental carRental) {
        CarRental carRental1 = new CarRental(carRental.getName(), carRental.getWebsite(), carRental.getPhoneNumber(),
                carRental.getOwner(), carRental.getLogoType(), carRental.getBranchList(), carRental.getCarList(),
                carRental.getEmployeeList());
        carRental1.setId(UUID.randomUUID());
        carRental = carRentalRepository.save(carRental1);
        return carRental;
    }

    @Transactional
    public CarRental getById(UUID id) {
        CarRental carRental = carRentalRepository.getCarRentalsById(id);
        return carRental;
    }

    //Nie wiem czy "deleteAll()" to dobre rozwizanie
    //Nie mogę wywołać metody "DeleteAll" bez stworzenia obiektu
    @Transactional
    public void deleteCarRental() {
        carRentalRepository.delete();
    }

    //Alternatywna metoda do delete
    //Usuniecie CarRental przy pomocy "id"
    @Transactional
    public void deleteCarRentalV2(UUID id) {
        carRentalRepository.deleteById(id);
    }

    /*@Transactional
    public void updateAddress(UUID id, AddressDto addressDto){
        Address address = new Address(id, addressDto.getStreet(), addressDto.getCity(),
                addressDto.getZipCode(),addressDto.getCountry());
        addressRepository.saveAndFlush(address);
    }*/
    @Transactional
    public void updateCarRentals(UUID id, CarRentalDto carRentalDto) {
        CarRental carRental = new CarRental(id, carRentalDto.getName(), carRentalDto.getWebsite(), carRentalDto.getPhoneNumber(),
                carRentalDto.getOwner(), carRentalDto.getLogoType());
        carRentalRepository.saveAndFlush(carRental);
    }

    public static CarRentalDto mapperToCarRentalDto(CarRental carRental) {
        return new CarRentalDto(carRental.getName(), carRental.getWebsite(), carRental.getPhoneNumber(),
                carRental.getOwner(), carRental.getLogoType(), BranchService.mapperToListRegisterBranch(carRental.getBranchList()),
                BranchService.mapperToListCarDto(carRental.getCarList()),
                EmployeeService.mapperToListEmployeeDto(carRental.getEmployeeList()));
    }


}
