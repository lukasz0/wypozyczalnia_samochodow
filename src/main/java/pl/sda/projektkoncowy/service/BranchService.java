package pl.sda.projektkoncowy.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.projektkoncowy.dto.*;
import pl.sda.projektkoncowy.entity.*;
import pl.sda.projektkoncowy.mapper.BranchMapper;
import pl.sda.projektkoncowy.mapper.CarMapper;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class BranchService {
    private final BranchRepository branchRepository;


    public BranchService(BranchRepository branchRepository) {
        this.branchRepository = Objects.requireNonNull(branchRepository);
    }

    @Transactional
    public BranchId addBranch(Address address) {

        Branch branch = new Branch(address);
        branchRepository.save(branch);
        return new BranchId(branch.getId());
    }


    @Transactional
    public List<RegisterBranchForm> getBranches() {
        List<Branch> branches=branchRepository.getBranches();
        return branches.stream()
                .map(branch -> new RegisterBranchForm(new AddressDto(branch.getAddress().getStreet(),
                        branch.getAddress().getCity(),branch.getAddress().getZipCode(),branch.getAddress().getCountry()),
                        mapEmployeeWithoutPassword( branch.getEmployeeList()),
                        mapperToListCarDto(branch.getCarList())))
                .collect(Collectors.toList());
    }


    public static List<EmployeeDto> mapEmployeeWithoutPassword(List<Employee> employees){
        return employees.stream()
                .map(employee -> new EmployeeDto(employee.getFirstName(),
                        employee.getLastName(),
                        employee.getJobTitle(),
                        BranchService.mapperToRegisterBranchFromBranchService( employee.getBranch())))
                      //  employee.getPassword()))
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteBranch(UUID id) {
        branchRepository.deleteById(id);
    }

    @Transactional
    public Branch getById(UUID id) {
        Branch branch = branchRepository.getBranchById(id);
        //RegisterBranchForm form = mapperToRegisterBranchFromBranchService(branch);
        return branch;
    }

    @Transactional
    public List<RegisterBranchForm> getByCity(String city) {
        List<Branch>  branchList = branchRepository.getByCity(city);
        return branchList.stream()
                .map(branch -> mapperToRegisterBranchFromBranchService(branch))
                .collect(Collectors.toList());
    }

    public static RegisterBranchForm mapperToRegisterBranchFromBranchService(Branch branch) {
        return new RegisterBranchForm(AddressService.mapperToAddressDto( branch.getAddress()),
                EmployeeService.mapperToListEmployeeDto( branch.getEmployeeList()),
                mapperToListCarDto(branch.getCarList()));
    }

    public static RegisterBranchForm mapperToRegisterBranchFromBranchService(Address address, List<Car> carList) {
        return new RegisterBranchForm(AddressService.mapperToAddressDto( address),
                mapperToListCarDto(carList));
    }

    public static List<CarDto> mapperToListCarDto(List<Car> carList){
        return carList.stream()
                .map(car-> CarService.mapCarToCarDto(car))
                .collect(Collectors.toList());
    }

    public static RegisterBranchForm mapperToRegisterBranch(Branch branch){
        return new RegisterBranchForm(AddressService.mapperToAddressDto( branch.getAddress()),
                EmployeeService.mapperToListEmployeeDto(branch.getEmployeeList()),
                BranchService.mapperToListCarDto(branch.getCarList()));
    }

    public static List<RegisterBranchForm> mapperToListRegisterBranch(List<Branch> branchList){
        return branchList.stream()
                .map(branch -> mapperToRegisterBranch(branch))
                .collect(Collectors.toList());
    }

    public static Branch mapperToBranch(RegisterBranchForm registerBranchForm) {
        return new Branch(AddressService.mapperToAddress(registerBranchForm.getAddressDto()),
                EmployeeService.mapperToListEmployee(registerBranchForm.getEmployeeListDto()),
                mapperToListCar(registerBranchForm.getCarListDto()));
    }

    public static List<Car> mapperToListCar(List<CarDto> carDtoList) {
        return carDtoList.stream()
                .map(carDto -> CarService.mapperToCar(carDto))
                .collect(Collectors.toList());

    }
}

