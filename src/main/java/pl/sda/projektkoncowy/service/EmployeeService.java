package pl.sda.projektkoncowy.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.projektkoncowy.dto.EmployeeDto;
import pl.sda.projektkoncowy.entity.Address;
import pl.sda.projektkoncowy.entity.Branch;
import pl.sda.projektkoncowy.entity.Employee;
import pl.sda.projektkoncowy.entity.EmployeeRepository;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository){
        this.employeeRepository= Objects.requireNonNull(employeeRepository);
    }


    @Transactional
    public List<EmployeeDto> getEmployees() {
        List<Employee> employees = employeeRepository.findAll();
        return mapperToListEmployeeDto(employees);
    }

    @Transactional
    public EmployeeDto addEmployee(EmployeeDto employeeDto, Branch branch) {
        Employee toEmployee = mapperToEmployee(employeeDto);
        toEmployee.setBranch(branch);
        Employee employee = employeeRepository.saveAndFlush(toEmployee);
        return mapperToEmployeeDto(employee);
    }

    @Transactional
    public void deleteEmployeeById(UUID id) {
        employeeRepository.deleteById(id);
    }

    public static Employee mapperToEmployee(EmployeeDto employeeDto) {
        return new Employee(employeeDto.getFirstName(),employeeDto.getLastName(),employeeDto.getJobTitle(), employeeDto.getPassword(),BranchService.mapperToBranch(employeeDto.getRegisterBranchForm()));
    }

    public static List<Employee> mapperToListEmployee(List<EmployeeDto> employeeDtoList){
        return employeeDtoList.stream()
                .map(employeeDto -> mapperToEmployee(employeeDto))
                .collect(Collectors.toList());
    }

    public static EmployeeDto mapperToEmployeeDto(Employee employee){
        return new EmployeeDto(employee.getFirstName(), employee.getLastName(), employee.getJobTitle(), "ukryte",BranchService.mapperToRegisterBranchFromBranchService( employee.getBranch().getAddress(),employee.getBranch().getCarList()));
    }

    public static List<EmployeeDto> mapperToListEmployeeDto(List<Employee> employeeList){
        return employeeList.stream()
                .map(employee -> mapperToEmployeeDto(employee))
                .collect(Collectors.toList());
    }


}
