package pl.sda.projektkoncowy.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.projektkoncowy.dto.AddressDto;
import pl.sda.projektkoncowy.dto.RegisterBranchForm;
import pl.sda.projektkoncowy.entity.Address;
import pl.sda.projektkoncowy.entity.AddressRepository;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AddressService {

    private final AddressRepository addressRepository;

    public AddressService(AddressRepository addressRepository){
        this.addressRepository= Objects.requireNonNull(addressRepository);
    }

    @Transactional
    public Address addAddress(RegisterBranchForm form){
        Address address = new Address(form.getAddressDto().getStreet(),form.getAddressDto().getCity(),form.getAddressDto().getZipCode(),form.getAddressDto().getCountry());
        addressRepository.save(address);
        return address;
    }

    @Transactional
    public Address addAddress(AddressDto addressDto){
        Address address = new Address(addressDto.getStreet(),addressDto.getCity(),addressDto.getZipCode(),addressDto.getCountry());
        addressRepository.save(address);
        return address;
    }

    @Transactional
    public List<AddressDto> getListAddresses(){
        List<Address> addressList = addressRepository.getListAddresses();
        return addressList.stream()
                .map(address->new AddressDto(address.getStreet(), address.getCity(),
                        address.getZipCode(),
                        address.getCountry()))
                .collect(Collectors.toList());
    }
    /*@Transactional
    public void updateAddress(UUID id, AddressDto addressDto) {
        addressRepository.updateListAddresses(addressDto.getStreet(), addressDto.getCity(), addressDto.getZipCode(),
                addressDto.getCountry(), id);

    }*/
    @Transactional
    public void updateAddress(UUID id, AddressDto addressDto){
        Address address = new Address(id, addressDto.getStreet(), addressDto.getCity(),
                addressDto.getZipCode(),addressDto.getCountry());
        addressRepository.saveAndFlush(address);
    }

    public static AddressDto mapperToAddressDto(Address address){
        return new AddressDto(address.getStreet(), address.getCity(),
                address.getZipCode(),
                address.getCountry());
    }

    public static List<AddressDto> mapperToListAddressesDto(List<Address> addressList){
        return addressList.stream()
                .map(address -> mapperToAddressDto(address))
                .collect(Collectors.toList());
    }

    public static Address mapperToAddress(AddressDto addressDto){
        return new Address(addressDto.getStreet(),addressDto.getCity(),addressDto.getZipCode(),addressDto.getCountry());
    }
}
