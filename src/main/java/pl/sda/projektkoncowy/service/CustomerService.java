package pl.sda.projektkoncowy.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.projektkoncowy.dto.CustomerDto;
import pl.sda.projektkoncowy.dto.RegisterCompanyForm;
import pl.sda.projektkoncowy.dto.RegisterPersonForm;
import pl.sda.projektkoncowy.entity.Address;
import pl.sda.projektkoncowy.entity.Company;
import pl.sda.projektkoncowy.entity.CustomerRepository;
import pl.sda.projektkoncowy.entity.Person;
import pl.sda.projektkoncowy.exception.EmailAlreadyExistsException;
import pl.sda.projektkoncowy.exception.PeselAlreadyExistsException;
import pl.sda.projektkoncowy.exception.VatAlreadyExistsException;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CustomerService {

    private  final CustomerRepository repository;
    private static ModelMapper mapper;

    public CustomerService (CustomerRepository repository,ModelMapper mapper){
        this.repository= Objects.requireNonNull(repository);
        this.mapper = Objects.requireNonNull(mapper);
    }

    @Transactional
    public CustomerDto registerCompany(RegisterCompanyForm form, Address address){
        if(repository.hasCustomerWithEmail(form.getEmail())){
            throw new EmailAlreadyExistsException("email already exists" + form.getEmail());
        }
        if (repository.hasCompanyWithVat(form.getVat())){
            throw new VatAlreadyExistsException("vat already exists: "+form.getVat());
        }

        final var company=new Company(form.getEmail(),AddressService.mapperToAddress( form.getAddressDto()), form.getName(), form.getVat(),form.getPassword());
        company.setAddresses(address);
        repository.save(company);
        return new CustomerDto(company.getId(),company.getEmail(),company.getName(),company.getVat(),AddressService.mapperToAddressDto( company.getAddresses()));
    }

    @Transactional(readOnly = true)
    public List<CustomerDto> listAllCustomers() {
        List<CustomerDto> customerDtos = repository.findAll().stream()
                .map(customer -> {
                    CustomerDto customerDto = new CustomerDto(customer.getId(), customer.getEmail(), AddressService.mapperToAddressDto(customer.getAddresses())
                            );
                    customer.getReservations().stream()
                            .forEach(reservation -> customerDto.getReservationDtos().add(ReservationService.mapperToReservationDto(reservation)));
                    return customerDto;
                })
                .collect(Collectors.toList());


        return customerDtos;
    }

    @Transactional(readOnly = true)
    public CustomerDto findById(UUID customerId) {
        final var customer=repository.getById(customerId);
        return new CustomerDto(customer.getId(),customer.getEmail(),
                AddressService.mapperToAddressDto(customer.getAddresses()));
    }

    @Transactional
    public CustomerDto registerPerson(RegisterPersonForm form, Address address) {
        if(repository.hasCustomerWithEmail(form.getEmail())){
            throw new EmailAlreadyExistsException("email already exists" + form.getEmail());
        }
        if (repository.hasCompanyWithVat(form.getPesel())){
            throw new PeselAlreadyExistsException("PESEL already esists: "+form.getPesel());
        }

        final var person=new Person(form.getEmail(), AddressService.mapperToAddress( form.getAddressDto()), form.getPassword(), form.getFirstName(),form.getLastName(), form.getPesel());
        person.setAddresses(address);
        repository.save(person);
        return new CustomerDto(person.getId(),person.getEmail(),person.getFirstName(), person.getLastName(), person.getPesel(),AddressService.mapperToAddressDto( person.getAddresses()));
    }


}
