package pl.sda.projektkoncowy.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.projektkoncowy.dto.CarDto;
import pl.sda.projektkoncowy.dto.CustomerDto;
import pl.sda.projektkoncowy.dto.ReservationDto;
import pl.sda.projektkoncowy.entity.*;
import pl.sda.projektkoncowy.exception.CustomerNotExistsException;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ReservationService {

    private final ReservationRepository reservationRepository;
    private final CarRepository carRepository;
    private final CustomerService customerService;
    private final CustomerRepository customerRepository;
    private final RentalService rentalService;
    private final ReturnService returnService;
    private static ModelMapper mapper;

    public ReservationService(ReservationRepository reservationRepository, ModelMapper mapper,CarRepository carRepository, CustomerService customerService,CustomerRepository customerRepository, RentalService rentalService, ReturnService returnService) {
        this.reservationRepository = reservationRepository;
        this.carRepository= Objects.requireNonNull(carRepository);
        this.customerService=Objects.requireNonNull(customerService);
        this.customerRepository= Objects.requireNonNull(customerRepository);
        this.rentalService = Objects.requireNonNull(rentalService);
        this.returnService=Objects.requireNonNull(returnService);
        this.mapper = mapper;
    }

    public static Reservation mapperTotReservation(ReservationDto reservationDto){
    Reservation reservation=mapper.map(reservationDto,Reservation.class);
    Customer customer = reservation.getCustomer();
    reservation.setCustomer(customer);
    return reservation;
    }

    public static List<Reservation> mapperToListReservation(List<ReservationDto> reservationDtoList){
    return reservationDtoList.stream()
            .map(reservationDto -> mapperTotReservation(reservationDto))
            .collect(Collectors.toList());
    }

    public static ReservationDto mapperToReservationDto(Reservation reservation){
        return mapper.map(reservation,ReservationDto.class);
    }

    public static List<ReservationDto> mapperToListReservationDto(List<Reservation> reservations){
        return reservations.stream()
                .map(reservation -> mapperToReservationDto(reservation))
                .collect(Collectors.toList());
    }

    @Transactional
    public List<ReservationDto> getReservations() {
        List<Reservation> reservations=reservationRepository.findAll();
        return mapperToListReservationDto(reservations);
    }

    @Transactional
    public ReservationDto addReservation(ReservationDto reservationDto, CarDto carDto, Branch branch) {
        //Reservation reservation=mapperTotReservation(reservationDto);
        Reservation reservation = new Reservation(
                reservationDto.getDateReservation(),
                carRepository.getById( reservationDto.getCarId()),
                reservationDto.getRentalFrom(),
                reservationDto.getRentalTo(),
                reservationDto.getIdBranchRental(),
                reservationDto.getIdBranchReturn(),
                reservationDto.getCost(),
                customerRepository.getById( reservationDto.getCustomerId())
        );

        //TODO: czy tak można robić - inaczej pojawia sie ze nie ustawiony recznie id, bierze przy mapowaniu id z dto a tam nie ma (można ewentualnie w DTO nadawać id)
        reservation.setIdReservation(UUID.randomUUID());
        Car car = CarService.mapperToCar(carDto, branch);
        reservation.setCar(car);

        Reservation reservation1 = ( reservationRepository.save(reservation));
        ReservationDto reservationDtoOut = new ReservationDto(
                reservation1.getIdReservation(),
                reservation1.getDateReservation(),
                reservation1.getCar().getId(),
                reservation1.getRentalFrom(),
                reservation1.getRentalTo(),
                reservation1.getIdBranchRental(),
                reservation1.getIdBranchReturn(),
                reservation1.getCost(),
                reservation1.getCustomer().getId());//dokończyć mapper do reservationDTO
//        CustomerDto customerDto=customerService.findById(reservation.getIdReservation());
//        Customer customer = mapper.map(customerDto,Customer.class);
        //TODO: CustomerOptional
//        Optional<Customer> customerOptional = customerRepository.findById(reservation.getIdReservation());
//        Customer customer = customerOptional.orElseThrow(
//                ()->new CustomerNotExistsException("Cannot found customer with that id: " + reservation.getIdReservation())
//        );
//        customer.getReservations().add(reservation);
//        customerRepository.save(customer);
        return reservationDtoOut;
    }

    @Transactional
    public ReservationDto getById(UUID id) {
        Reservation reservation=reservationRepository.getById(id);
        return mapperToReservationDto(reservation);
    }

    @Transactional
    public List<ReservationDto> getByCarId(UUID id) {
        Car car = carRepository.getById(id);
        List<Reservation> reservations = reservationRepository.getByCar(car);
        return mapperToListReservationDto(reservations);
    }

    @Transactional
    public List<ReservationDto> getByCustomerId(UUID id) {
        Optional<Customer> customerOptional = customerRepository.findById(id);
        Customer customer = customerOptional.orElseThrow(
                ()->new CustomerNotExistsException("Cannot found customer with that id: " +id)
        );
        List<Reservation> reservations = reservationRepository.getByCustomer(customer);
        return mapperToListReservationDto(reservations);
    }
}
