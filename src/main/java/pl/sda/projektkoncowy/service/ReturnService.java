package pl.sda.projektkoncowy.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.projektkoncowy.dto.ReturnDto;
import pl.sda.projektkoncowy.entity.Return;
import pl.sda.projektkoncowy.entity.ReturnRepository;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ReturnService {

    private final ReturnRepository returnRepository;
    private final ModelMapper mapper;

    public ReturnService(ReturnRepository returnRepository,ModelMapper mapper) {
        this.returnRepository = Objects.requireNonNull( returnRepository);
        this.mapper= Objects.requireNonNull(mapper);
    }

    @Transactional
    public List<ReturnDto> getAll() {
        List<Return> returns=returnRepository.findAll();
        return mapperToListReturnDto(returns);
    }

    @Transactional
    public ReturnDto addReturn(ReturnDto returnDto) {
        Return returnIn = mapperToReturn(returnDto);
        returnIn.setReturnId(UUID.randomUUID());
        Return returnOut=returnRepository.save(returnIn);
        return mapperToReturnDto(returnOut);
    }

    public ReturnDto mapperToReturnDto(Return returnIn){
        return mapper.map(returnIn,ReturnDto.class);
    }

    public List<ReturnDto> mapperToListReturnDto(List<Return> returns) {
    return returns.stream()
            .map(r->mapperToReturnDto(r))
            .collect(Collectors.toList());
    }



    public Return mapperToReturn(ReturnDto returnDto) {
    return mapper.map(returnDto,Return.class);
    }
}
