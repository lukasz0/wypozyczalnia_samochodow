package pl.sda.projektkoncowy.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.projektkoncowy.dto.AvailableCarsDto;
import pl.sda.projektkoncowy.dto.CarDto;
import pl.sda.projektkoncowy.entity.Branch;
import pl.sda.projektkoncowy.entity.Car;
import pl.sda.projektkoncowy.entity.CarRepository;
import pl.sda.projektkoncowy.entity.Reservation;
import pl.sda.projektkoncowy.enumarated.Status;
import pl.sda.projektkoncowy.mapper.CarMapper;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CarService {
    private final CarRepository carRepository;
    private static ModelMapper mapper;


    public CarService(CarRepository carRepository, ModelMapper mapper) {
        this.carRepository = Objects.requireNonNull(carRepository);
        this.mapper = mapper;
    }

    @Transactional
    public List<CarDto> getCars() {
        List<Car> cars = carRepository.findAll();
        return BranchService.mapperToListCarDto(cars);
    }

    @Transactional
    public CarDto addCar(CarDto carDto, Branch branch) {
        Car car = mapperToCar(carDto, branch);
        Car carOut = carRepository.save(car);
        return mapCarToCarDto(carOut);
    }

    public static CarDto mapCarToCarDto(Car carOut) {
        CarDto carDto = mapper.map(carOut, CarDto.class);
        return carDto;
    }

    public static Car mapperToCar(CarDto carDto) {

        return new Car(carDto.getCarBrand(), carDto.getModel(), carDto.getBodyType(), carDto.getYear(),
                carDto.getColor(), carDto.getMileage(),
                carDto.getPrice(), ReservationService.mapperToListReservation(carDto.getReservationListDto()),
                BranchService.mapperToBranch(carDto.getRegisterBranchForm()), carDto.getStatus(),
                carDto.getCategory());
    }

    public static Car mapperToCar(CarDto carDto, Branch branch) {
        if (carDto.getId() == null) {

            return new Car(carDto.getCarBrand(), carDto.getModel(), carDto.getBodyType(), carDto.getYear(),
                    carDto.getColor(), carDto.getMileage(),
                    carDto.getPrice(), ReservationService.mapperToListReservation(carDto.getReservationListDto()),
                    branch, carDto.getStatus(),
                    carDto.getCategory());
        } else {
            return new Car(carDto.getId(), carDto.getCarBrand(), carDto.getModel(), carDto.getBodyType(), carDto.getYear(),
                    carDto.getColor(), carDto.getMileage(),
                    carDto.getPrice(), ReservationService.mapperToListReservation(carDto.getReservationListDto()),
                    branch, carDto.getStatus(),
                    carDto.getCategory());
        }
    }


    @Transactional
    public CarDto getById(UUID id) {
        Car car = carRepository.getById(id);
        return mapCarToCarDto(car);
    }

    @Transactional
    public List<CarDto> getAvailableCars(AvailableCarsDto availableCarsDto) {

        List<Car> cars = carRepository.findAll();
        List<Reservation> reservationList = cars.stream()
                .filter(car -> !car.getReservationList().isEmpty())
                .flatMap(car -> car.getReservationList().stream())
                .filter(reservation -> availableCarsDto.getRentalFrom().isAfter(reservation.getRentalFrom().toLocalDate()) && availableCarsDto.getRentalFrom().isBefore(reservation.getRentalTo().toLocalDate()))
                .collect(Collectors.toList());

        List<Car> carsAvailable = reservationList.stream()
                .map(reservation -> reservation.getCar())
                .collect(Collectors.toList());

        cars.forEach(car -> car.setStatus(Status.AVAILABLE));

        for (int i = 0; i < carsAvailable.size(); i = i + 1) {
            int finalI = i;
            cars.forEach(car -> {
                        if (Objects.equals(car, carsAvailable.get(finalI))){
                            car.setStatus(Status.NOAVAILABLE);
                        }  ;
                    });
        }
            return BranchService.mapperToListCarDto(cars);
    }
}
