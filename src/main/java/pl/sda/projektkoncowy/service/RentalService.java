package pl.sda.projektkoncowy.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.projektkoncowy.dto.RentalDto;
import pl.sda.projektkoncowy.entity.Rental;
import pl.sda.projektkoncowy.entity.RentalRepository;
import pl.sda.projektkoncowy.entity.Reservation;
import pl.sda.projektkoncowy.entity.ReservationRepository;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class RentalService {

    private final RentalRepository rentalRepository;
    private final ReservationRepository reservationRepository;
    private static ModelMapper mapper;

    public RentalService(RentalRepository rentalRepository, ModelMapper mapper, ReservationRepository reservationRepository) {
        this.rentalRepository = Objects.requireNonNull( rentalRepository);
        this.mapper=Objects.requireNonNull(mapper);
        this.reservationRepository=Objects.requireNonNull(reservationRepository);
    }

    @Transactional
    public List<RentalDto> getAll() {
        List<Rental> rentals = rentalRepository.findAll();
        return mapperToListRentalDto(rentals);
    }

    public RentalDto mapperToRentalDto(Rental rental){
        return mapper.map(rental,RentalDto.class);
    }

    private List<RentalDto> mapperToListRentalDto(List<Rental> rentals) {
        return rentals.stream()
                .map(rental -> mapperToRentalDto(rental))
                .collect(Collectors.toList());
    }

    @Transactional
    public RentalDto addRental(RentalDto rentalDto) {

        //Rental rental=mapperToRental(rentalDto);
        Reservation reservation=reservationRepository.getById(rentalDto.getReservationID());
        Rental rental=new Rental(rentalDto.getRentalDate(),reservation,rentalDto.getComments());
        rental.setIdRental(UUID.randomUUID());
        Rental rentalOut=rentalRepository.save(rental);
        reservation.setRentalCar(rental);
        reservationRepository.save(reservation);

        return mapperToRentalDto(rentalOut);
    }

    private Rental mapperToRental(RentalDto rentalDto) {
        return mapper.map(rentalDto,Rental.class);
    }
}
