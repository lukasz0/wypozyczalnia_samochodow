package pl.sda.projektkoncowy.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import pl.sda.projektkoncowy.dto.RegisterBranchForm;
import pl.sda.projektkoncowy.entity.Branch;


@Mapper
public interface BranchMapper {

    BranchMapper INSTANCE = Mappers.getMapper(BranchMapper.class);

    @Mappings({
            @Mapping(target = "addressDto", source = "address"),
            @Mapping(target = "employeeListDto", source = "employeeList"),
            @Mapping(target = "carListDto", source = "carList")
    })
    RegisterBranchForm mapperToRegisterBranchForm(Branch source);
}
