package pl.sda.projektkoncowy.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import pl.sda.projektkoncowy.dto.CarDto;

import pl.sda.projektkoncowy.entity.Car;


@Mapper
public interface CarMapper {

    CarMapper INSTANCE = Mappers.getMapper( CarMapper.class );

    @Mappings({
            @Mapping(target = "id", source = "id"),
            @Mapping(target = "carBrand", source = "carBrand"),
            @Mapping(target = "model", source = "model"),
            @Mapping(target = "bodyType", source = "bodyType"),
            @Mapping(target = "year", source = "year"),
            @Mapping(target = "color", source = "color"),
            @Mapping(target = "mileage", source = "mileage"),
            @Mapping(target = "price", source = "price"),
            @Mapping(target = "reservationList", source = "reservationList"),
            @Mapping(target = "status", source = "status"),
            @Mapping(target = "category", source = "category"),
    })
     CarDto mapCarToCarDto(Car source);

}
