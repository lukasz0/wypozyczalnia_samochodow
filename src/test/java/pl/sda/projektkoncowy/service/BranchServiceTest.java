package pl.sda.projektkoncowy.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.sda.projektkoncowy.dto.AddressDto;
import pl.sda.projektkoncowy.dto.CarDto;
import pl.sda.projektkoncowy.dto.EmployeeDto;
import pl.sda.projektkoncowy.dto.RegisterBranchForm;
import pl.sda.projektkoncowy.entity.Address;
import pl.sda.projektkoncowy.entity.BranchRepository;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class BranchServiceTest {
    @Autowired
    private BranchService branchService;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private AddressService addressService;

    @Test
    void addBranch(){
        AddressDto addressDto=new AddressDto("Sw. Marcin","Poznan","60-100","Poland");
        List<CarDto> carDtoList = new ArrayList<>();
        List<EmployeeDto> employeeDtoList = new ArrayList<>();
        RegisterBranchForm form =new RegisterBranchForm(addressDto, employeeDtoList,carDtoList);

        Address address= addressService.addAddress(form);
        branchService.addBranch(address);
    }


}